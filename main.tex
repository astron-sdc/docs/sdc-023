% documentclass{} takes the following mutually-exclusive options:
%
%  note      -- formats the document as a technical note.
%  proposed  -- formats the document as a proposed change to the baseline.
%  baseline  -- formats the document as part of the technical baseline.
\documentclass[note,short]{astron}
\usepackage{savetrees}
\setlength{\parindent}{0cm}

\input{meta}
\input{changes}

\setDocTitle{SDC History}
\setDocProgram{SDC}
\setDocAuthors{
  \addPerson{John D. Swinbank}{ASTRON}{\vcsDate}
}

% These are set according to information obtained from make and git.
\setDocNumber{\docHandle}
\setDocRevision{\vcsRevision}
\setDocDate{\vcsDate}


\begin{document}
\maketitle

\paragraph{Introduction}
ASTRON's Science Data Centre is developing, distributing, supporting, and operating a range of software and services that provide data archiving, processing, discovery, and analysis across ASTRON's portfolio of instrumentation.
Our twin goals ensuring that an appropriate data infrastructure exists to enable the widest possible community to generate high-impact science with LOFAR2.0 data, and working with our partners worldwide to develop the Regional Centre Network that will provide similar services for the SKA.

\paragraph{Organization and Staffing}
As part of the “ASTRON2.0” reorganization of summer 2020, the SDC was arranged into two parallel activity tracks: an \emph{operational unit}, SDCO, under Head of Operations Roberto Pizzo, and a \emph{development programme}, SDCP.
The SDC System Architect, Hanno Holties, is under SDCO line management, but with responsibilities to both development and operations.
In November 2020, John Swinbank was appointed SDCP Programme Manager.

SDCP draws staff from across the I\&S competence groups.
It currently consists of $\sim$16 FTEs, of which four are recent (post-2020) hires; one more will join in April 2023.
External contractors fill some development and organizational roles.

SDCO is a line management group within A\&O, with eight currently staff under management including one recent hire.

Since 2021, SDC development has been organized using a variation on the Scaled Agile Framework\footnote{SAFE; \url{https://scaledagileframework.com}} which is becoming increasingly prevalent in medium and large software development organizations.
The increasing adoption of agile techniques has been successful in increasing responsiveness to requests and developer engagement; we continue to develop and refine this approach.

\paragraph{Archive Services}
The SDC maintains and develops LOFAR (LTA) and Apertif (ALTA) archives.
Milestones include:

\begin{itemize}

  \item{
    Publishing major data releases, notably LoTSS DR2\footnote{\url{https://science.astron.nl/sdc/astron-data-explorer/data-releases/lotss-dr2/}} and Apertif DR1\footnote{\url{https://science.astron.nl/sdc/astron-data-explorer/data-releases/apertif-dr1/}}.
    In addition to their intrinsic scientific value, these efforts were important demonstration of supporting archiving advanced (“science-ready”) data products in the archive environment.
  }

  \item{
    Demonstration, as part of the ESCAPE H2020 Project\footnote{\url{https://projectescape.eu}}, of geographically-transparent access to and processing of LOFAR data using a continent-scale “data lake”.
  }

\end{itemize}

Future goals include expansion to additional archive sites and compliance with the FAIR\footnote{Findable, Accessible, Interoperable, Reusable; \url{https://go-fair.org}} principles.

\paragraph{Scientific Pipelines}
The SDC is responsible for the imaging pipeline “components” --- the various executables, performing tasks such as flagging, calibration, and imaging, that can be composed to form imaging pipelines --- that were developed by ASTRON over the last several years.
These tools are widely used across the LOFAR community, forming the basis of external efforts like the ddf-pipeline\footnote{\url{https://github.com/mhardcastle/ddf-pipeline}} used for processing LoTSS data.
We have successfully built close relationships with the user community and regard these tools as one of the most important pure-software contributions to the continued success of LOFAR as a telescope.

ASTRON is now building on those components to develop an in-house standardized \& supported imaging capability.
The first pipeline, LINC\footnote{LOFAR Initial Calibration; \url{https://linc.readthedocs.io/}} --- building on earlier work on the Prefactor pipeline, and providing direction-independent calibration and imaging --- was released in mid-2022, and has seen wide update across the community.
The second pipeline, Rapthor,\footnote{\url{https://rapthor.readthedocs.io/}} will see an initial release in the first half of 2023.

\paragraph{Data Processing}

The SDC has developed ATDB,\footnote{Originally “Apertif Task Database”, but the name is now vestigial} a system for managing in-archive data processing.
During 2021 and 2022, this was developed in close collaboration between the SDCO and SDCP teams to provide flexible mechanisms for specifying, executing, and monitoring pipelines processing data drawn from the LOFAR LTA.
This system became operational in early 2023, and is being used for reprocessing of legacy LOFAR data to reduce its size and increase its scientific usefulness (the LDV project).

Work is now ongoing to expand the ATDB system to manage processing of data across all LTA sites.

\paragraph{Data Discovery \& Analysis}

The SDC adopted on IVOA\footnote{International Virtual Observatory Alliance; \url{https://www.ivoa.net/}} standards as the basis for future data releases; some data is already available through \url{https://vo.astron.nl}.
Since 2021, we host the NL-VO\footnote{\url{https://www.virtualobservatory.nl}} and are actively involved IVOA's Radio Astronomy Interest Group.

Work began in 2022 on ADEX, a  new web interface to ASTRON's data holdings, providing a modernized look and feel, a upgraded capabilities (e.g. all-sky visualization), and improved accessibility.
Prototypes of ADEX are now available, with a first release due in 2023.

ASTRON led ESCAPE WP5, developing the ESFRI Science Analysis Platform.\footnote{ESAP; \url{https://git.astron.nl/astron-sdc/escape-wp5/esap-api-gateway/-/wikis/home}}
ESAP provides a modular system that makes it possible to integrate data discovery, data analysis (e.g. Jupyter\footnote{\url{https://jupyter.org}} notebooks), and batch computing into a unified platform.
ESAP was delivered at the conclusion of ESCAPE in January 2023, and is being evaluated by major facilities including the CTA Observatory and SKA Regional Centres Network.
Future SDC work on interactive analysis will build on ESAP.

\paragraph{SKA Regional Centres}

The SDC team has also been active in planning for the SKA Regional Centre Network --- the internationally-distributed network of data centres that will ingest, archive, and support astronomers in analyzing the $\sim$1\,EB per year of science-ready data products that the SKA Observatory will generate starting later this decade.
Specifically:

\begin{itemize}
\item{
  During 2021, the SKA Regional Centres Steering Committee constituted a series of working groups to define the requirements for the SRC Network.
  Through these groups, SDC staff played a major role in shaping the SRC Network as it is now envisioned.
}
\item{
  Prototyping of the SRC Network began in 2022, with ASTRON coordinating the Dutch contribution; in particular, ASTRON staff are leading the development of the SRC Network science analysis platform, basing it in part on ESAP.
}
\item{
  SDC staff also sit on the SRC Network Architecture Forum, defining the overall architecture of the SRC system, and participate at a variety of levels throughout the SRC planning process.
}

\end{itemize}

This effort will feed into future LOFAR and Apertif data services as well as the SRC Network.




%/*
%Sponsored projects:
%
%- ESCAPE
%- EGI-ACE
%- DICE
%- EOSC-Hub
%- ORP
%- LDV
%- FuSE
%- EOSC Future
%
%*/
%
%Major activities:
%
%- LDV
%- SDF-PPE
%- Focus Project
%- SRC Network
%- Pipeline development (LINC, Rapthor)
%- Science platforms and interface development
%
%The same framing as the L2 vision?
%
%- Archives
%- Pipelines
%- Processing Infrastructure
%- Data Discovery
%
%Staffing
%
%- Programme Manager, System Architect, etc
%- New hires: Janneke, Fanna, Klaas, Sangeeth


\end{document}
